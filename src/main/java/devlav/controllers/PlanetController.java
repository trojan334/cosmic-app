package devlav.controllers;


import devlav.models.Planet;
import devlav.models.dtos.PlanetDto;
import devlav.services.PlanetService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class PlanetController {

    private PlanetService planetService;

    public PlanetController(PlanetService planetService) {
        this.planetService = planetService;
    }

    @GetMapping("/planets")
    public List<Planet> getPlanets() {
        return planetService.getPlanets();
    }

    /*
     *
     *  DTO
     *
     */

//    @GetMapping("/dto/planets")
//    public List<PlanetDto> getPlanetsDto() {
//        return planetService.getPlanetsDto();
//    }

    @PostMapping("/dto/planets")
    public Planet addPlanet(@RequestBody PlanetDto planetDto) {
        return planetService.addPlanet(planetDto);
    }

    @PutMapping("/dto/planets")
    public void updatePlanet(@RequestBody PlanetDto planetDto) {
        planetService.updatePlanet(planetDto);
    }

    @DeleteMapping("/dto/planets/{planetName}")
    public void deletePlanet(@PathVariable String planetName) {
        planetService.deletePlanet(planetName);
    }

    @GetMapping("/dto/planets")
    public List<PlanetDto> getPlanetsDto(@RequestParam(value = "distance", required = false) Long distance) {
        if (distance != null && distance > 0) {
            return planetService.getPlanetByDistanceFromSun(distance);
        }
        return planetService.getPlanetsDto();
    }

    @Controller
    public static class HomeController {

        @GetMapping("/")
        public String homePage(){
            return "index";
        }

    }
}
