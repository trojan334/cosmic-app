package devlav.repositories;

import devlav.models.Planet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlanetRepository extends JpaRepository<Planet,Long> {

    @Query("select p from Planet p where p.planetName = ?1")//JPQL
    Optional<Planet> findPlanetByPlanetName(String planetName);

    @Transactional
    @Modifying
    @Query("select p from Planet p where p.planetName = ?1")//JPQL
    void deletePlanetByPlanetName(String planetName);


    @Query("select p from Planet p where p.planetName <= ?1")//JPQL
    List<Planet> findPlanetsByDistanceFromSun(Long distance);
}
